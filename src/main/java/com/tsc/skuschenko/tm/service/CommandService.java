package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.ICommandService;
import com.tsc.skuschenko.tm.model.Command;
import com.tsc.skuschenko.tm.api.ICommandRepository;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
