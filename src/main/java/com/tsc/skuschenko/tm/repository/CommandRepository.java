package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.ICommandRepository;
import com.tsc.skuschenko.tm.constant.ArgumentConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConst.TM_ABOUT,
            ArgumentConst.ARG_ABOUT,
            "show about information"
    );

    private static final Command HELP = new Command(
            TerminalConst.TM_HELP,
            ArgumentConst.ARG_HELP,
            "show help information"
    );

    private static final Command VERSION = new Command(
            TerminalConst.TM_VERSION,
            ArgumentConst.ARG_VERSION,
            "show version application"
    );

    private static final Command EXIT = new Command(
            TerminalConst.TM_EXIT,
            null,
            "close program"
    );

    private static final Command INFO = new Command(
            TerminalConst.TM_INFO,
            ArgumentConst.ARG_INFO,
            "close program"
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.TM_ARGS,
            null,
            "show arguments"
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.TM_COMMANDS,
            null,
            "show commands"
    );

    public static final Command[] TERMINAL_COMMANDS = new Command[]{
            ABOUT, HELP, VERSION, EXIT, INFO, ARGUMENTS, COMMANDS
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
