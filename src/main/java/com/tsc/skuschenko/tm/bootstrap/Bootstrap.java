package com.tsc.skuschenko.tm.bootstrap;

import com.tsc.skuschenko.tm.api.ICommandController;
import com.tsc.skuschenko.tm.api.ICommandRepository;
import com.tsc.skuschenko.tm.api.ICommandService;
import com.tsc.skuschenko.tm.constant.ArgumentConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.controller.CommandController;
import com.tsc.skuschenko.tm.repository.CommandRepository;
import com.tsc.skuschenko.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private  final ICommandRepository commandRepository = new CommandRepository();

    private  final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run (final String... args){
        System.out.println("***Welcome to task manager***");
        if (parseArgs(args)) commandController.exit();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Enter command:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg.toLowerCase()) {
            case ArgumentConst.ARG_ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.ARG_VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ARG_HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.showSystemInfo();
                break;
            default:
                commandController.showWrongArg(arg);
                break;
        }
    }

    public void parseCommand(final String command) {
        if (command == null) return;
        switch (command.toLowerCase()) {
            case TerminalConst.TM_ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.TM_VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.TM_HELP:
                commandController.showHelp();
                break;
            case TerminalConst.TM_EXIT:
                commandController.exit();
                break;
            case TerminalConst.TM_INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.TM_COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.TM_ARGS:
                commandController.showArgs();
                break;
            default:
                commandController.showWrongCmd(command);
                break;
        }
    }

    public   boolean parseArgs(String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

}
