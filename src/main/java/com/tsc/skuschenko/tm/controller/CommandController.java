package com.tsc.skuschenko.tm.controller;

import com.tsc.skuschenko.tm.api.ICommandController;
import com.tsc.skuschenko.tm.api.ICommandService;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.model.Command;
import com.tsc.skuschenko.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showWrongCmd(final String cmd) {
        System.out.print(cmd + " command doesn't  found. ");
        System.out.println("Pleas print '" + TerminalConst.TM_HELP + "' for more information");
    }

    @Override
    public void showWrongArg(final String arg) {
        System.out.print(arg + " argument doesn't  found. ");
        System.out.println("Pleas print '" + TerminalConst.TM_HELP + "' for more information");
    }

    @Override
    public void showAbout() {
        System.out.println("[" + TerminalConst.TM_ABOUT.toUpperCase() + "]");
        System.out.println("AUTHOR: Semyon Kuschenko");
        System.out.println("EMAIL: skushchenko@tsconsulting.com");
    }

    @Override
    public void showVersion() {
        System.out.println("[" + TerminalConst.TM_VERSION.toUpperCase() + "]");
        System.out.println("1.0.0");
    }

    @Override
    public void showHelp() {
        System.out.println("[" + TerminalConst.TM_HELP.toUpperCase() + "]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands)
            System.out.println(command);
    }

    @Override
    public void showSystemInfo() {
        final long availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final boolean isMaxMemory = maxMemory == Long.MAX_VALUE;
        final String maxMemotyVal = (isMaxMemory ? "no limit" : NumberUtil.formatSize(maxMemory));
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("[" + TerminalConst.TM_INFO.toUpperCase() + "]");
        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Free memory: " + NumberUtil.formatSize(freeMemory));
        System.out.println("Maximum memory: " + maxMemotyVal);
        System.out.println("Total memory available to JVM: " + NumberUtil.formatSize(totalMemory));
        System.out.println("Used memory by JVM: " + NumberUtil.formatSize(usedMemory));
        System.out.println("[OK]");
    }

    @Override
    public void showCommands() {
        System.out.println("[" + TerminalConst.TM_COMMANDS.toUpperCase() + "]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            final String name = command.getName();
            if (name == null) continue;
            System.out.println(name);
        }
    }

    @Override
    public void showArgs() {
        System.out.println("[" + TerminalConst.TM_ARGS.toUpperCase() + "]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            final String arg = command.getArg();
            if (arg == null) continue;
            System.out.println(arg);
        }
    }

    @Override
    public void exit() {
        System.exit(0);
    }

}
