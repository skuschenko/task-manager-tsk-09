package com.tsc.skuschenko.tm.api;

public interface ICommandController {

    void showWrongCmd(final String cmd);

    void showWrongArg(final String arg);

    void showAbout();

    void showVersion();

    void showHelp();

    void showSystemInfo();

    void showCommands();

    void showArgs();

    void exit();

}
