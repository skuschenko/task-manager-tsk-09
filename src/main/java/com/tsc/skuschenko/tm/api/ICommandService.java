package com.tsc.skuschenko.tm.api;

import com.tsc.skuschenko.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
